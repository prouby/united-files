/* file.c --- United files file functions.
 *
 * Copyright (C) 2019  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "../include/unitedfiles.h"

#include "file.h"
#include "alloc.h"

void
_get_file_info (file_t file)
{
  struct stat info;

  if (file->fp != NULL)
    {
      int fd = fileno (file->fp);

      if (fstat (fd, &info) != 0)
        {
          exit (EXIT_FAILURE);
        }
    }
  else
    {
      if (stat (file->path, &info) != 0)
        {
          exit (EXIT_FAILURE);
        }
    }

  file->mode = (uint64_t) info.st_mode;
  file->size = (uint64_t) info.st_size;
  file->access = (uint64_t) info.st_atim.tv_sec;
  file->modification = (uint64_t) info.st_mtim.tv_sec;
}

void
open_ro_fp (file_t file)
{
  if (file->fp != NULL)
    return;

  file->fp = fopen (file->path, "r");
  if (file->fp == NULL)
    {
      exit (EXIT_FAILURE);
    }
}

file_t
open_file (const char * file_name)
{
  file_t file = NULL;
  MALLOC (file, sizeof (struct s_file_t));

  MALLOC (file->path, sizeof(char) * (strlen (file_name) + 1));
  strcpy (file->path, file_name);

  file->fp = NULL;
  file->status = FILE_STATUS_ON_DISQUE;

  _get_file_info (file);

  return file;
}

file_t
create_file (const char * path,
             uint64_t pos,
             uint64_t mode,
             uint64_t size,
             uint64_t access,
             uint64_t modification)
{
  file_t file = NULL;
  MALLOC (file, sizeof (struct s_file_t));

  MALLOC (file->path, sizeof(char) * (strlen (path) + 1));
  strcpy (file->path, path);

  file->fp = NULL;
  file->pos = pos;
  file->mode = mode;
  file->size = size;
  file->access = access;
  file->modification = modification;
  file->status = FILE_STATUS_ON_ARCHIVE;

  return file;
}

void
unindex_file (file_t file)
{
  FREE(file->path);
  file->path = NULL;
}

char *
get_file_path (file_t file)
{
  return file->path;
}

void
write_file_on_stream (file_t src, FILE * dest)
{
  if (src->path == NULL)
    return;

  if (src->fp == NULL)
      open_ro_fp (src);

  size_t readed;
  char * buffer = NULL;
  MALLOC(buffer, sizeof (char) * UF_BUFFER_SIZE);

  uint64_t mode = htole64(src->mode);
  uint64_t size = htole64(src->size);
  uint64_t access = htole64(src->access);
  uint64_t modification = htole64(src->modification);

  fwrite (src->path, sizeof (char), strlen (src->path) + 1, dest);
  fwrite (&mode, sizeof (uint64_t), 1, dest);
  fwrite (&size, sizeof (uint64_t), 1, dest);
  fwrite (&access, sizeof (uint64_t), 1, dest);
  fwrite (&modification, sizeof (uint64_t), 1, dest);

  /* FIXME: Check error */
  while ((readed = fread (buffer, sizeof (char), UF_BUFFER_SIZE, src->fp)) != 0)
    fwrite (buffer, sizeof (char), readed, dest);

  FREE (buffer);
}

void
close_file (file_t file)
{
  if (file != NULL && file->fp != NULL)
    {
      fflush (file->fp);
      fclose (file->fp);
    }

  if (file != NULL && file->path != NULL)
    free(file->path);

  FREE (file);
}
