/* hashtable.c --- United files file functions.
 *
 * Copyright (C) 2020  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include "hashtable.h"

/*********/
/* TYPES */
/*********/

struct s_hashtable {
  // Data
  unsigned int size;
  void ** key;
  void ** data;

  // Functions
  hash_function hash;
  next_function next;
  cmp_function cmp;
};


/********************/
/* PUBLIC FUNCTIONS */
/********************/

hashtable
ht_create(unsigned int size,
          hash_function f_hash,
          next_function f_next,
          cmp_function f_cmp)
{
  hashtable ht = (hashtable) malloc(sizeof(*ht));
  if (ht == NULL)
    return NULL;

  ht->key = (void *) malloc(sizeof(void *) * size);
  if (ht->key == NULL)
    {
      free(ht);
      return NULL;
    }

  ht->data = (void *) malloc(sizeof(void *) * size);
  if (ht->data == NULL)
    {
      free(ht);
      return NULL;
    }

  for (int i = 0; i < size; i++)
    ht->data[i] = NULL;

  ht->size = size;
  ht->hash = f_hash;
  ht->next = f_next;
  ht->cmp = f_cmp;

  return ht;
}

void
ht_free(hashtable ht)
{
  if (ht == NULL) return;

  if (ht->key != NULL)
    {
      free(ht->key);
      ht->key = NULL;
    }

  if (ht->data != NULL)
    {
      free(ht->data);
      ht->data = NULL;
    }

  free(ht);
}

unsigned int
ht_size(hashtable ht)
{
  return ht->size;
}

void *
ht_find(hashtable ht, void * key)
{
  if (ht == NULL || key == NULL) return NULL;

  ht_hash h = ht->hash(ht, key);
  void * finded = NULL;

  unsigned int x = ht->size;
  while (x > 0)
    {
      if (ht->key[h] != NULL &&
          ht->cmp(ht->key[h], key) == 0)
        {
          finded = ht->data[h];
          break;
        }
      h = ht->next(ht, h);
      x--;
    }

  return finded;
}

ht_bool
ht_insert(hashtable ht, void * key, void * data)
{
  if (ht == NULL || key == NULL) return HT_BOOL_FALSE;

  ht_hash h = ht->hash(ht, key);

  unsigned int x = ht->size;
  while (x > 0)
    {
      if (ht->key[h] == NULL ||
          ht->cmp(ht->key[h], key) == 0)
        {
          ht->key[h] = key;
          ht->data[h] = data;
          return HT_BOOL_TRUE;
        }
      h = ht->next(ht, h);
      x--;
    }

  return HT_BOOL_FALSE;
}

ht_bool
ht_delete(hashtable ht, void *key)
{
  if (ht == NULL || key == NULL) return HT_BOOL_FALSE;

  ht_hash h = ht->hash(ht, key);

  unsigned int x = ht->size;
  while (x > 0)
    {
      if (ht->key[h] != NULL &&
          ht->cmp(ht->key[h], key) == 0)
        {
          ht->key[h] = NULL;
          ht->data[h] = NULL;
          return HT_BOOL_TRUE;
        }
      h = ht->next(ht, h);
      x--;
    }

  return HT_BOOL_FALSE;
}

void
ht_foreach(hashtable ht, foreach_function f, void *data)
{
  unsigned int x = ht_size(ht);
  unsigned int i = 0;
  while(x > 0) {
    if (ht->key[i] != NULL)
      f(ht->key[i], ht->data[i], data);
    i++; x--;
  }
}

void
ht_map(hashtable ht, map_function f, void *data)
{
  unsigned int x = ht_size(ht);
  unsigned int i = 0;
  while(x > 0) {
    if (ht->key[i] != NULL)
      ht->data[i] = f(ht->key[i], ht->data[i], data);
    i++; x--;
  }
}


hashtable
_ht_duplicate(hashtable ht, unsigned int new_size)
{
  hashtable new_ht = ht_create(new_size,
                               ht->hash,
                               ht->next,
                               ht->cmp);

  unsigned int x = ht->size;
  unsigned int i = 0;
  while (x > 0) {
    // No available size
    if (i >= new_size) {
      ht_free(new_ht);
      return NULL;
    }

    if (ht->key[i] != NULL)
      ht_insert(new_ht, ht->key[i], ht->data[i]);
    i++; x--;
  }

  return new_ht;
}

hashtable
ht_duplicate(hashtable ht)
{ return _ht_duplicate(ht, ht->size); }

hashtable
ht_duplicate_new_size(hashtable ht, unsigned int size)
{ return _ht_duplicate(ht, size); }
