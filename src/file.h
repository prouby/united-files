/* file.h --- United files file functions.
 *
 * Copyright (C) 2019  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#ifndef __FILE_H__
#define __FILE_H__

#include <stdbool.h>

#define FILE_STATUS_ON_DISQUE 0
#define FILE_STATUS_ON_ARCHIVE 1

struct s_file_t
{
  char *   path;                /* Path */
  FILE *   fp;                  /* File pointer */
  int      status;              /* File status */
  uint64_t pos;                 /* Pos on archive */
  uint64_t mode;                /* File mode */
  uint64_t size;                /* Size of the file */
  uint64_t access;              /* Last access time */
  uint64_t modification;        /* Last modification time */
};
typedef struct s_file_t * file_t;

file_t open_file (const char * file_name);
file_t create_file (const char * path,
                    uint64_t pos,
                    uint64_t mode,
                    uint64_t size,
                    uint64_t access,
                    uint64_t modification);
char * get_file_path (file_t file);
void unindex_file (file_t file);
void write_file_on_stream (file_t src, FILE * dest);
void close_file (file_t file);

#endif /* __FILE_H__ */
