/* file_list.c --- United files file functions.
 *
 * Copyright (C) 2019  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>

#include "../include/unitedfiles.h"

#include "hashtable.h"
#include "archive.h"
#include "error.h"
#include "alloc.h"
#include "file.h"

file_list
_rec_table_to_list (file_t *tab, int file_number)
{
  if (file_number < 0)
    return NULL;

  file_list l = malloc (sizeof(struct s_file_list));;
  if (l == NULL)
    return (void *)UF_ERR_SYSTEM;

  l->name = (const char *)tab[file_number]->path;
  l->id   = file_number;
  l->next = _rec_table_to_list(tab, file_number - 1);

  return l;
}

// TODO: Make it thread safe
int _tab_current_index = 0;

void
_ht_foreach_fill_tab(void *key, void *value, void *data)
{
  file_t *tab = (file_t *)data;

  tab[_tab_current_index++] = (file_t)value;
}

file_list
uf_file_list (ufa ar)
{
  int x = ht_size(ar->files);
  file_t *tab = NULL;
  MALLOC(tab, x * sizeof(**tab));

  // Fill table
  ht_foreach(ar->files, _ht_foreach_fill_tab, (void *)tab);
  _tab_current_index = 0;

  // To list
  file_list ret = _rec_table_to_list(tab, ar->file_number - 1);

  FREE(tab);
  return ret;
}

void
uf_free_file_list (file_list l)
{
  if (l == NULL)
    return;

  file_list next = l->next;
  free(l);

  uf_free_file_list (next);
}
