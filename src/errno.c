/* errno.h --- United files errno functions.
 *
 * Copyright (C) 2019  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "../include/unitedfiles.h"

#define INVALIDE_ERROR_CODE "Invalide error code"

const char *
uf_strerrno (int err)
{
  static char * strerr[] =
    {
     "Success",
     "Unknown",
     "Feature not implemented yet",
     "Archive is on read only mode",
     "Archive format",
     "No file on arrchive"
    };

  if (err >= UF_ERR_CODE_MAX)
    return INVALIDE_ERROR_CODE;

  return strerr[err];
}
