/* alloc.h --- United files archive functions.
 *
 * Copyright (C) 2019  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#ifndef __ALLOC_H__
#define __ALLOC_H__

#define MALLOC_CHECK(ptr)                                               \
  if (ptr == NULL)                                                      \
    {                                                                   \
      fprintf (stderr,                                                  \
               "[UnitedFiles] Allocation failed: %s: %s: %d\n",         \
               __FILE__, __func__, __LINE__);                           \
      exit (EXIT_FAILURE);                                              \
    }

#define MALLOC(ptr, size)                       \
  do                                            \
    {                                           \
      ptr = malloc (size);                      \
      MALLOC_CHECK (ptr);                       \
    }                                           \
  while (0);

#define REALLOC(ptr, size)                      \
  do                                            \
    {                                           \
      ptr = realloc (ptr, size);                \
      MALLOC_CHECK (ptr);                       \
    }                                           \
  while (0);

#define FREE(ptr)                               \
  do {                                          \
    if (ptr != NULL)                            \
      free (ptr);                               \
  } while (0);

#endif /* __ALLOC_H__ */
