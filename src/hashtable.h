/* hashtable.h --- United files file functions.
 *
 * Copyright (C) 2020  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#ifndef HASHTABLE_H
#define HASHTABLE_H

#define HT_BOOL_TRUE 0
#define HT_BOOL_FALSE 1

/*********/
/* TYPES */
/*********/

typedef struct s_hashtable * hashtable;

typedef unsigned short ht_bool;
typedef unsigned int   ht_hash;

typedef ht_hash (*hash_function)    (hashtable, void *);
typedef ht_hash (*next_function)    (hashtable, ht_hash);
typedef ht_bool (*cmp_function)     (void *, void *);
typedef void    (*foreach_function) (void *, void *, void *);
typedef void *  (*map_function)     (void *, void *, void *);

/*************/
/* PROTOTYPE */
/*************/

hashtable ht_create (unsigned int  size,
                     hash_function f_hash,
                     next_function f_next,
                     cmp_function  f_cmp);
void ht_free (hashtable ht);

unsigned int ht_size   (hashtable ht);
void *    ht_find      (hashtable ht, void * key);
ht_bool   ht_insert    (hashtable ht, void * key, void * data);
ht_bool   ht_delete    (hashtable ht, void * key);
void      ht_foreach   (hashtable ht, foreach_function f, void * data);
void      ht_map       (hashtable ht, map_function f, void * data);
hashtable ht_duplicate (hashtable ht);
hashtable ht_duplicate_new_size (hashtable ht, unsigned int size);

#endif /* HASHTABLE_H */
