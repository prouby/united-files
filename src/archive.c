/* archive.h --- United files archive functions.
 *
 * Copyright (C) 2019  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <endian.h>
#include <errno.h>
#include <stdint.h>

#include <sys/stat.h>

#include "../include/unitedfiles.h"

#include "hashtable.h"
#include "archive.h"
#include "option.h"
#include "alloc.h"
#include "file.h"

/*********/
/* Macro */
/*********/

#define _archive_is_readonly(ar)                \
  as_option (ar->options, UF_OPTION_READONLY)

#define UNITED_ARCHIVE_HEADER "UNITED"

/********/
/* Type */
/********/

#define SIZE_UFA() (sizeof (struct s_archive))

/********************/
/* Private function */
/********************/

ht_hash
_ht_files_hash (hashtable ht, void * key)
{
  char * name = (char *) key;

  ht_hash x = 0;

  int i = 0;
  char c = name[i];
  while (c != '\0')
    {
      x += (ht_hash) c;
      c = name[++i];
    }

  x %= ht_size(ht);

  return x;
}

ht_hash
_ht_files_next (hashtable ht, ht_hash prev)
{
  ht_hash next = (prev + 1) % ht_size(ht);
  return next;
}

ht_bool
_ht_files_cmp (void * k1, void *k2)
{
  return strcmp((char *)k1, (char *)k2);
}

void
_ht_foreach_commit(void *key, void *value, void *data)
{
  file_t file = (file_t)value;
  FILE * dest = (FILE *)data;
  write_file_on_stream (file, dest);
}

void
_ht_foreach_close(void *key, void *value, void *data)
{
  close_file((file_t)value);
}

/* Allocated new ufa struct, and set default value. */
ufa
_calloc_archive (const char * path)
{
  ufa ar = NULL;
  MALLOC(ar, SIZE_UFA());


  MALLOC(ar->path, sizeof(char *) * (strlen(path) + 1));
  strcpy (ar->path, path);

  ar->file = NULL;
  ar->commited_on_disk = false;

  ar->options = UF_OPTION_DEFAULT;

  ar->file_number = 0;
  ar->_files_allocated_size = 8;
  ar->files = ht_create(8,
                        _ht_files_hash,
                        _ht_files_next,
                        _ht_files_cmp);

  return ar;
}

void
_open_archive_file_pointeur (ufa ar, char * mode)
{
  if (ar->file != NULL)
    fclose (ar->file);

  if (mode == NULL)
    {
      if (_archive_is_readonly(ar) ||
          access (ar->path, W_OK) != 0)
        mode = "r";
      else
        mode = "r+";
    }

  ar->file = fopen (ar->path, mode);
  if (ar->file == NULL)
    {
      fprintf (stderr, "[UnitedFiles] Failed open archive file.\n");
      exit (EXIT_FAILURE);
    }
}

void
_write_archive_header (ufa ar)
{
  if (_archive_is_readonly(ar))
    return;

  if (ar->file == NULL)
    _open_archive_file_pointeur(ar, "w");

  rewind (ar->file);
  fwrite ("UNITED", sizeof (char), 7, ar->file);
}

bool
_read_archive_header (ufa ar)
{
  _open_archive_file_pointeur (ar, "r");

  char buf[7];

  if (fread (buf, sizeof (char), 7, ar->file) < 7)
    return false;

  return (strcmp (buf, UNITED_ARCHIVE_HEADER) == 0);
}

file_t
_read_file_header_on_archive (ufa ar)
{
  if (ar->file == NULL)
    _open_archive_file_pointeur (ar, "r");

  if (feof (ar->file) != 0)
    return NULL;

  char * name = NULL;
  MALLOC(name, sizeof (char) * UF_BUFFER_SIZE);

  uint64_t mode = 0;
  uint64_t size = 0;
  uint64_t access = 0;
  uint64_t modification = 0;

  int i = 0;
  char last;
  do
    {
      last = (char)fgetc (ar->file);
      name[i++] = last;

      if (i > UF_BUFFER_SIZE)
        {
          uf_errno = UF_ERR_ARCHIVE_FORMAT;
          FREE(name);
          return NULL;
        }
    } while (last != '\0');

  fread (&mode, sizeof (uint64_t), 1, ar->file);
  fread (&size, sizeof (uint64_t), 1, ar->file);
  fread (&access, sizeof (uint64_t), 1, ar->file);
  fread (&modification, sizeof (uint64_t), 1, ar->file);

  mode = le64toh (mode);
  size = le64toh (size);
  access = le64toh (access);
  modification = le64toh (modification);

  file_t nf = create_file (name, ftell (ar->file), mode,
                           size, access, modification);

  FREE (name);

  /* Go to next file */
  fseek (ar->file, (long)size, SEEK_CUR);
  return nf;
}

int
_write_archive_file_on_disk (ufa ar, file_t src, const char * dest_path)
{
  FILE * dest = fopen (dest_path, "w+");
  if (dest == NULL)
      return UF_ERR_SYSTEM;

  if (ar->file == NULL)
     _open_archive_file_pointeur (ar, "r");

  fseek (ar->file, (long)src->pos, SEEK_SET);

  size_t size = 0;
  size_t cnt_size = 0;
  char * buffer = NULL;
  MALLOC (buffer, sizeof (char) * UF_BUFFER_SIZE);

  do
    {
      cnt_size += UF_BUFFER_SIZE;
      if (cnt_size > (size_t)src->size)
        size = UF_BUFFER_SIZE - (cnt_size - src->size);
      else
        size = UF_BUFFER_SIZE;

      size = fread (buffer, sizeof (char), size, ar->file);
      if (size == 0 && ferror(ar->file) != 0)
        return UF_ERR_SYSTEM;

      size = fwrite (buffer, sizeof (char), size, dest);
      if (size == 0 && ferror(dest) != 0)
        return UF_ERR_SYSTEM;
    }
  while (cnt_size <= (size_t)src->size);

  fflush (dest);

  fclose (dest);
  FREE (buffer);

  return UF_ERR_SUCCESS;
}

int
_rec_create_sub_dir (char * path)
{
  int ret = UF_ERR_SUCCESS;
  char * cur = strrchr (path, '/');

  if (cur != NULL)
    {
      cur[0] = '\0';
      ret = _rec_create_sub_dir(path);
      cur[0] = '/';
    }

  if (access(path, F_OK) != 0)
    if (mkdir (path, 0777) == -1)
        return UF_ERR_SYSTEM;

  return ret;
}

int
_create_sub_dir (const char * path)
{
  char * s = NULL;
  MALLOC (s, sizeof (char) * strlen (path));
  strcpy (s, path);

  char * c = strrchr(s, '/');

  c[0] = '\0';
  int ret = _rec_create_sub_dir (s);

  FREE(s);
  return ret;
}

/********************/
/* Public functions */
/********************/

void
uf_init ()
{
  uf_errno = 0;
}

ufa
uf_open (const char * name)
{
  if (access(name, R_OK) != 0)
    return NULL;

  ufa ar = _calloc_archive (name);
  file_t file = NULL;

  ar->commited_on_disk = true;
  add_option (ar->options, UF_OPTION_READONLY);

  if (!_read_archive_header (ar))
    {
      uf_errno = UF_ERR_ARCHIVE_FORMAT;
      FREE(ar);
      return NULL;
    }

  while (1)
    {
      file = _read_file_header_on_archive (ar);
      if (file == NULL)
        break;

      if (ar->file_number >= ar->_files_allocated_size)
        {
          ar->_files_allocated_size *= 2;
          hashtable n = ht_duplicate_new_size (ar->files, ar->_files_allocated_size);
          ht_free(ar->files);
          ar->files = n;
        }

      ht_insert(ar->files,
                (void *)get_file_path(file),
                (void *)file);
      ar->file_number++;
    }

  return ar;
}

ufa
uf_create (const char * name)
{
  return _calloc_archive (name);
}

int
uf_add_file (ufa ar, const char * file)
{
  ar->commited_on_disk = false;

  file_t new_file = open_file (file);

  if (ar->file_number >= ar->_files_allocated_size)
    {
      ar->_files_allocated_size *= 2;
      hashtable n = ht_duplicate_new_size (ar->files, ar->_files_allocated_size);
      ht_free(ar->files);
      ar->files = n;
    }

  ht_insert(ar->files,
            (void *)get_file_path(new_file),
            (void *)new_file);
  ar->file_number++;

  return UF_ERR_SUCCESS;
}

int
uf_remove_file (ufa ar, const char * file)
{
  if (ht_delete(ar->files, (void *)file) == HT_BOOL_FALSE)
    return UF_ERR_NO_FILE_ON_AR;
  return UF_ERR_SUCCESS;
}

int
uf_extract_file (ufa ar, const char * path, const char * new)
{
  int x;
  file_t file;

  file = ht_find(ar->files, (void *)path);

  if (strchr(new, '/') != NULL)
    if ((x = _create_sub_dir (new)) != UF_ERR_SUCCESS)
      return x;

  if (file == NULL)
    return UF_ERR_NO_FILE_ON_AR;

  x = _write_archive_file_on_disk (ar, file, new);

  chmod (new, (mode_t)(file->mode & ~S_IFREG));

  return x;
}

int
uf_commit (ufa ar)
{
  if (_archive_is_readonly (ar))
    return UF_ERR_READONLY;

  _create_sub_dir (ar->path);   /* Create destination dirs if needed */
  _write_archive_header (ar);

  ht_foreach(ar->files, _ht_foreach_commit, (void *)ar->file);

  return UF_ERR_SUCCESS;
}

void
uf_close (ufa ar)
{
  if (ar == NULL)
    return;

  FREE (ar->path);

  if (ar->file != NULL)
    {
      fflush (ar->file);
      fclose (ar->file);
    }

  // Close all files
  ht_foreach(ar->files, _ht_foreach_close, NULL);

  ht_free(ar->files);
  free (ar);
}


void
uf_reset_options (ufa ar)
{
  reset_option (ar->options);
}

void
uf_add_options (ufa ar, int options)
{
  add_option (ar->options, options);
}
