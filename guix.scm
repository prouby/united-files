;;; Copyright (C) 2019 Pierre-Antoine Rouby <contact@parouby.fr>

(use-modules ((guix licenses) #:prefix l:)
             (guix packages)
             (guix download)
             (guix gexp)
             (guix utils)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages pkg-config)
             (gnu packages check)
             (gnu packages guile))

(package
  (name "unitedfiles")
  (version "0.0.1")
  (source (local-file "." "unitedfiles" #:recursive? #t))
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf"   ,autoconf)
     ("automake"   ,automake)
     ("libtool"    ,libtool)
     ("pkg-config" ,pkg-config)
     ("check"      ,check)))
  (inputs
   `(("guile" ,guile-2.2)))
  (synopsis "A minimalist archive library")
  (description
   "United files is a minimalist archive library.")
  (home-page "https://framagit.org/prouby/united-files")
  (license l:gpl3+))
