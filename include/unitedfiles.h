/* unitedfiles.h --- United files API.
 *
 * Copyright (C) 2019  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

/**
 * @file unitedfiles.h
 * @brief The unitied files API header.
 */

#ifndef __UNITEDFILES_H__
#define __UNITEDFILES_H__

/**
 * @def UF_BUFFER_SIZE
 * @brief United files buffer size.
 */
#define UF_BUFFER_SIZE 512

/***********/
/* Options */
/***********/

/**
 * @def UF_OPTION_DEFAULT
 * @brief Default option.
 */
#define UF_OPTION_DEFAULT 0x0000

/**
 * @def UF_OPTION_READONLY
 * @brief Option for readonly archive.
 */
#define UF_OPTION_READONLY 0x0001


/**********/
/* Errors */
/**********/

/**
 * @def UF_ERR_SUCCESS
 * @brief Success error code.
 */
#define UF_ERR_SUCCESS 0

/**
 * @def UF_ERR_UNKNOWN
 * @brief Unknown error code.
 */
#define UF_ERR_UNKNOWN 1

/**
 * @def UF_ERR_NOT_IMPLEMENTED
 * @brief Not implemented feature error code.
 */
#define UF_ERR_NOT_IMPLEMENTED 2

/**
 * @def UF_ERR_READONLY
 * @brief Readonly error code.
 */
#define UF_ERR_READONLY 3

/**
 * @def UF_ERR_ARCHIVE_FORMAT
 * @brief Archive format error code.
 */
#define UF_ERR_ARCHIVE_FORMAT 4

/**
 * @def UF_ERR_NO_FILE_ON_AR
 * @brief No file on archive error code.
 */
#define UF_ERR_NO_FILE_ON_AR 5

/**
 * @def UF_ERR_SYSTEM
 * @brief System error code. (See system errno.)
 */
#define UF_ERR_SYSTEM 6

/**
 * @def UF_ERR_CODE_MAX
 * @brief Max err code.
 */
#define UF_ERR_CODE_MAX 7

/*******************/
/* Global variable */
/*******************/

/**
 * @var int uf_errno
 * @brief Error code variable.
 */
int uf_errno;

/********/
/* Type */
/********/

/**
 * @typedef typedef struct s_archive * ufpa;
 * @brief Archive pointer.
 */
typedef struct s_archive * ufa;

/**
 * @typedef typedef struct s_file_list * file_list;
 * @brief File list pointer.
 */
typedef struct s_file_list * file_list;

/**
 * @struct s_file_list
 * @brief File chained list structure.
 */
struct s_file_list
{
  const char * name;
  int          id;
  file_list    next;
};

/*************/
/* Functions */
/*************/

/**
 * @fn void uf_init ()
 * @brief Init lib
 */
void uf_init ();

/**
 * @fn ufa archive_open (const char * name)
 * @brief Open a united files archive.
 *
 * @param name Path of file.
 * @return New archive pointer.
 */
ufa uf_open (const char * name);

/**
 * @fn ufa uf_create (const char * name)
 * @brief Create new archive.
 *
 * @param name Path of file.
 * @return New archive pointer.
 */
ufa uf_create (const char * name);

/**
 * @fn void uf_reset_options (ufa ar)
 * @brief Reset archive options.
 *
 * @param ar Archive pointer.
 */
void uf_reset_options (ufa ar);

/**
 * @fn void uf_add_options (ufa ar, int options)
 * @brief Add options for archive.
 *
 * @param ar Archive pointer.
 * @param options Options.
 */
void uf_add_options (ufa ar, int options);

/**
 * @fn char * uf_add_file (ufa ar, const char * file)
 * @brief Add file on archive.
 *
 * @param ar Archive pointer.
 * @param file Path of file.
 * @return On success, return zero, else return uf_errno value.
 */
int uf_add_file (ufa ar, const char * file);

/**
 * @fn int uf_remove_file (ufa ar, const char * file)
 * @brief Remove file on archive.
 *
 * @param ar Archive pointer.
 * @param file Path of file.
 * @return On success, return zero, else return uf_errno value.
 */
int uf_remove_file (ufa ar, const char * file);

/**
 * @fn file_list uf_file_list (ufa ar)
 * @brief Return file list.
 *
 * @param ar Archive pointer.
 * @return Return file list.
 */
file_list uf_file_list (ufa ar);

/**
 * @fn void uf_free_file_list (ufa ar)
 * @brief Free file list.
 *
 * @param l file list.
 */
void uf_free_file_list (file_list l);

/**
 * @fn char * uf_extract_file (ufa ar, char * file)
 * @brief Extract file of archive.
 *
 * @param ar Archive pointer.
 * @param path Path of file in archive.
 * @param new Buffer for files system file path.
 * @return On success, return zero, else return uf_errno value.
 */
int uf_extract_file (ufa ar, const char * path, const char * new);

/**
 * @fn int uf_commit (ufa ar)
 * @brief Commit archive changement.
 *
 * @param ar Archive pointer.
 * @return On success, return zero, else return uf_errno value.
 */
int uf_commit (ufa ar);

/**
 * @fn void uf_close (ufa ar)
 * @brief Close archive.
 *
 * @param ar Archive pointer.
 */
void uf_close (ufa ar);

/**
 * @fn const char * uf_strerrno (int err)
 * @brief Get error message.
 *
 * @param err uf_errno code.
 * @return Return error message.
 */
const char * uf_strerrno (int err);

#endif /* __UNITEDFILES_H__ */
