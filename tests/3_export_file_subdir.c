#include <check.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <errno.h>

#include "../include/unitedfiles.h"

int ret;
ufa ar;

int
_cmp_file (const char *src, const char *dest)
{
  struct stat s_info, d_info;

  stat (src,  &s_info);
  stat (dest, &d_info);

  if (s_info.st_mode != d_info.st_mode ||
      s_info.st_size != d_info.st_size)
    return EXIT_FAILURE;

  return EXIT_SUCCESS;
}

START_TEST (export_file_on_sub_dirs)
{
  int x;
  uf_init ();

  ar = uf_open ("tmp/archive.ufa");
  ck_assert (ar != NULL);

  x = uf_extract_file (ar, "libunitedfiles.so",
                       "tmp/sys/lib/libunitedfiles.so");
  ck_assert (x == 0);


  x = _cmp_file ("libunitedfiles.so",
                 "tmp/sys/lib/libunitedfiles.so");
  ck_assert (x == 0);

  x = uf_extract_file (ar, "include/unitedfiles.h",
                       "tmp/sys/includes/unitedfiles.h");
  ck_assert (x == 0);

  x = _cmp_file ("include/unitedfiles.h",
                 "tmp/sys/includes/unitedfiles.h");
  ck_assert (x == 0);

  uf_close (ar);
}
END_TEST

int main (void)
{
  return 0;
}
