#include <check.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <errno.h>

#include "../include/unitedfiles.h"

int travel_list(file_list lst)
{
  if (lst == NULL)
    return 0;
  return 1 + travel_list(lst->next);
}


int ret;
ufa ar;

START_TEST (get_file_list)
{
  int x;
  file_list lst = NULL;
  uf_init ();

  ar = uf_open ("tmp/archive.ufa");
  ck_assert (ar != NULL);

  lst = uf_file_list (ar);
  ck_assert (lst != NULL && lst != (void*)UF_ERR_SYSTEM);

  x = travel_list(lst);
  ck_assert (0 != NULL);

  uf_free_file_list (lst);
  uf_close (ar);
}
END_TEST

int main (void)
{
  return 0;
}
