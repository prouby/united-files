#include <check.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <errno.h>

#include "../include/unitedfiles.h"

int ret;
ufa ar;

int
_cmp_file (const char *src, const char *dest)
{
  struct stat s_info, d_info;

  stat (src,  &s_info);
  stat (dest, &d_info);

  if (s_info.st_mode != d_info.st_mode ||
      s_info.st_size != d_info.st_size)
    return EXIT_FAILURE;

  return EXIT_SUCCESS;
}

START_TEST (export_file)
{
  int x;
  uf_init ();

  ar = uf_open ("tmp/archive.ufa");
  ck_assert (ar != NULL);

  x = uf_extract_file (ar, "unitedfiles.a", "tmp/unitedfiles.a");
  ck_assert (x == 0);

  x = _cmp_file ("unitedfiles.a", "tmp/unitedfiles.a");
  ck_assert (x == EXIT_SUCCESS);

  uf_close (ar);
}
END_TEST

int main (void)
{
  return 0;
}
