;;; unitedfiles.scm --- United files bindings.

;;; Copyright (C) 2019  Pierre-Antoine Rouby

;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (unitedfiles)
  #:use-module (system foreign)
  #:use-module (ice-9 iconv)
  #:export (uf-init
            uf-open
            uf-create
            uf-reset-options
            uf-add-options
            uf-add-file
            uf-remove-file
            uf-extract-file
            uf-commit
            uf-close))

(define %lib (dynamic-link "./libunitedfiles.so"))

(define lib-func
  (lambda (ret func-name args-type)
    (pointer->procedure ret
                        (dynamic-func func-name %lib)
                        args-type)))

(define-syntax-rule (define-lib-func name ret func-name args-type)
  (define name
    (lib-func ret func-name args-type)))


(define-public UF-BUFFER-SIZE         512)

(define-public UF-OPTION-DEFAULT      #x0000)
(define-public UF_OPTION_READONLY     #x0001)

(define-public UF-ERR-SUCCESS         0)
(define-public UF-ERR-UNKNOWN         1)
(define-public UF-ERR-NOT-IMPLEMENTED 2)
(define-public UF-ERR-READONLY        3)
(define-public UF-ERR-ARCHIVE-FORMAT  4)
(define-public UF-ERR-NO-FILE-ON-AR   5)
(define-public UF-ERR-SYSTEM          6)
(define-public UF-ERR-CODE-MAX        7)

(define-lib-func b-uf-init
  void "uf_init" '())

(define-lib-func b-uf-open
  '* "uf_open" '(*))

(define-lib-func b-uf-create
  '* "uf_create" '(*))

(define-lib-func b-uf-reset-options
  void "uf_reset_options" '(*))

(define-lib-func b-uf-add-options
  void "uf_add_options" (list '* int))

(define-lib-func b-uf-add-file
  int "uf_add_file" '(* *))

(define-lib-func b-uf-remove-file
  int "uf_remove_file" '(* *))

(define-lib-func b-uf-extract-file
  int "uf_extract_file" '(* * *))

(define-lib-func b-uf-commit
  int "uf_commit" '(*))

(define-lib-func b-uf-close
  void "uf_close" '(*))

(define-lib-func b-uf-extract-file
  '* "uf_strerrno" (list int))

(define-wrapped-pointer-type <ufa>
  ufa?
  wrap-ufa
  unwrap-ufa
  (lambda (context port)
    (format port "#<ufa ~x>"
            (pointer-address (unwrap-ufa context)))))

;;;
;;; Public
;;;

(define (uf-init)
  (b-uf-init))

(define (uf-open path)
  (let* ((str (bytevector->pointer (string->bytevector path "utf-8")))
         (ptr (b-uf-open str)))
    (if (null-pointer? ptr)
        #f (wrap-ufa ptr))))

(define (uf-create path)
  (let* ((str (bytevector->pointer (string->bytevector path "utf-8")))
         (ptr (b-uf-create str)))
    (if (null-pointer? ptr)
        #f (wrap-ufa ptr))))

(define (uf-reset-options ar)
  (if (ufa? ar)
      (let ((ptr (unwrap-ufa ar)))
        (b-uf-reset-options ptr))
      #f))

(define (uf-add-options ar opt)
  (if (and (ufa? ar) (number? opt))
      (let ((ptr (unwrap-ufa ar)))
        (b-uf-reset-options ptr))
      #f))

(define (uf-add-file ar path)
  (if (ufa? ar)
      (let ((ptr (unwrap-ufa ar))
            (str (bytevector->pointer (string->bytevector path "utf-8"))))
        (b-uf-add-file ptr str))
      #f))

(define (uf-remove-file ar path)
  (if (ufa? ar)
      (let ((ptr (unwrap-ufa ar))
            (str (bytevector->pointer (string->bytevector path "utf-8"))))
        (b-uf-remove-file ptr ptr))
      #f))

(define (uf-extract-file ar src-path dest-path)
  (if (ufa? ar)
      (let ((ptr (unwrap-ufa ar))
            (str-src (bytevector->pointer (string->bytevector src-path "utf-8")))
            (str-dest (bytevector->pointer (string->bytevector dest-path "utf-8"))))
        (b-uf-remove-file ptr str-src str-dest))
      #f))

(define (uf-commit ar)
  (if (ufa? ar)
      (let ((ptr (unwrap-ufa ar)))
        (b-uf-commit ptr))
      #f))

(define (uf-close ar)
  (if (ufa? ar)
      (let ((ptr (unwrap-ufa ar)))
        (b-uf-close ptr)
        #t)
      #f))
